﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI.Models
{
    #region Roles

    public enum UserRole
    {
        Administrator, ProductManager, RegularUser
    }

    #endregion

    public class Role
    {
        #region Properties

        public Int32 Id { get; set; }
        public UserRole UserRole { get; set; }

        #endregion
    }
}