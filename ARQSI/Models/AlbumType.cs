﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI.Models
{
    #region AlbumTypes

    public enum Type
    {
        CD, Vinyl
    }

    #endregion

    public class AlbumType
    {     
        #region Properties

        public Int32 Id { get; set; }
        public Type Type { get; set; }

        #endregion
    }
}