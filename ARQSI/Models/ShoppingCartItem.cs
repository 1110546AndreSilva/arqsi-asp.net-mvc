﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace ARQSI.Models
{
    public class ShoppingCartItem
    {
        #region Properties
        
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        public Int32 ShoppingCartID { get; set; }

        [Required]
        public Int32 AlbumID { get; set; }

        [Required]
        public virtual Album Album { get; set; }

        [Required]
        [Range(0, Int32.MaxValue, ErrorMessage = "The product's quantity is invalid.")]
        public Int32 Quantity { get; set; }

        #endregion
    }
}