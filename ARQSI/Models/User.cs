﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ARQSI.Models
{
    public class User
    {
        #region Properties

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        [Required]
        [MinLength(5, ErrorMessage="Username must have at least 5 characters.")]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string email { get; set; }

        public string PasswordHash { get; set; }

        public Guid API_Key { get; set; }

        [Required]
        public DateTime RegisterDate { get; set; }

        public DateTime LastLogin { get; set; }

        [Required]
        public Int32 RoleID { get; set; }

        public virtual Role Role { get; set; }

        public int ShoppingCartID { get; set; }

        public virtual ShoppingCart ShoppingCart { get; set; }

        #endregion
    }
}