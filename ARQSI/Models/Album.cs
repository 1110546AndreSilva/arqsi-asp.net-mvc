﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations; 

namespace ARQSI.Models
{
    public class Album
    {
        #region Properties

        public Int32 Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage="The album's name must not exceed 100 characters.")]
        public string AlbumName { get; set; }

        [Required]
        [StringLength(50, ErrorMessage="The artist's name must not exceed 50 characters.")]
        public string AlbumArtist { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage="Invalid tracks count.")]
        public Int32 NumberTracks { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage="The album's price is invalid.")]
        public double Price { get; set; }

        public Int32 AlbumTypeID { get; set; }
        public virtual AlbumType AlbumType{ get; set; }

        #endregion
    }
}