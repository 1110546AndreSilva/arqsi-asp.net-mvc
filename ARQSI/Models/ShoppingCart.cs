﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARQSI.Models
{
    public class ShoppingCart
    {
        #region Properties

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id;

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public DateTime LastUpdate { get; set; }

        public List<ShoppingCartItem> Items { get; set; }

        public double TotalPrice { get; set; }

        [Required]
        public Int32 UserID { get; set; }

        [Required]
        public virtual User User { get; set; }

        #endregion
    }
}